# nike-assignment
For the assignment I chose to work with python3 and pyspark.

To install the necessary components use venv to create a fresh environment.
```
    python3 -m venv nike
    source nike/bin/activate
```

Now that you have a clean environment install the components
```
    pip3 install -r requirements.txt
```

After installation make sure you have the necessary csv files in a folder and created a destination folder.

Now run the application using the following command:
```
    python3 src/app.py --source *source folder* --dest *destination folder*
```

And check the json output in the folder