import pyspark
import shutil
from pyspark.sql import SparkSession, functions
import pyspark.sql.functions as F

class spark_etl:
    def __init__(self):
        self.spark = SparkSession.builder \
            .master("local[*]") \
            .appName("nike_etl") \
            .getOrCreate()

    """
        Extract method (main method)
        --------------
            - Load csv's into spark dataframes
            - Join the dataframes

            - Return pandas dataframe
        --------------
    """
    def extract(self, source, destination):
        shutil.rmtree(destination)
        (calendar, product, sales, store) = self.load(source)
        calendar = self.transform(calendar)
        joined_df = self.join(calendar, product, sales, store)
        joined_df = self.fill_mising_weeks(joined_df, calendar)
        output = self.aggregate(joined_df)
        output.write.format('json').save(destination)
        return True

    """
        Load method
        -----------
            - Load the csv's from the data folder
        -----------
    """
    def load(self, location):
        calendar = self.spark.read.format("csv").option("header", "true").load(f"{location}/calendar.csv")
        product = self.spark.read.format("csv").option("header","true").load(f"{location}/product.csv")
        sales = self.spark.read.format("csv").option("header","true").load(f"{location}/sales.csv")
        store = self.spark.read.format("csv").option("header","true").load(f"{location}/store.csv")
        return (calendar, product, sales, store)

    """
        Transform method
        ----------------
            - Enriches the calendar dataframe to add week in the year
        ----------------
    """
    def transform(self, calendar):
        # Get the first value in calendar as date:
        first_row = calendar.first()
        first_date = f"{first_row.datecalendaryear}-01-{first_row.datecalendarday}"

        # Set the week of the year
        calendar = calendar.withColumn("dayofyear", \
                        (calendar["datekey"].cast("double")-F.lit(first_row.datekey)).cast("int"))

        calendar = calendar.withColumn("date",F.expr(f"date_add('{first_date}',dayofyear)"))
        return calendar.withColumn("weekofyear",F.weekofyear("date"))
    
    """
        Join method
        -----------
            - Join all columns to one dataframe
            - Add the unique key
        -----------
    """
    def join(self, calendar, product, sales, store):
        joined_df = calendar \
                    .join(sales, calendar.datekey == sales.dateId, "inner") \
                    .join(product, sales.productId == product.productid, "inner") \
                    .join(store, sales.storeId==store.storeid, "inner") \
                    .filter("weeknumberofseason is not null")

        joined_df = joined_df.withColumn("unique_key", \
                        F.concat(F.col('datecalendaryear'), \
                        F.lit("_"),F.col('channel'), \
                        F.lit("_"),F.col('division'), \
                        F.lit("_"),F.col('gender'), \
                        F.lit("_"),F.col('category')))
        joined_df = joined_df.select("unique_key","channel","division","gender","category","weekofyear","netSales","salesUnits")
        return joined_df
    
    """
        Fill method
        -----------
            - After the groupby and creating the unique key, some weeks are missing
            - This method does a crossjoin, to combine possible weeks and the unique_key
        -----------
    """
    def fill_mising_weeks(self, joined_df, calendar):
        poss = joined_df.select("unique_key","channel","division","gender","category") \
                .distinct()\
                .crossJoin(calendar.select('weekofyear').distinct())

        joined_df = joined_df.join(poss,\
                on=["unique_key","channel","division","gender","category","weekofyear"], \
                how="right").fillna({"netSales":0,"salesUnits":0})

        joined_df = joined_df\
                .withColumn("netSales",F.col("netSales").cast("double"))\
                .withColumn("salesUnits",F.col("salesUnits").cast("double"))
        return joined_df


    """
        Aggregate method
        -----------------
            - Group by and create list with week number, netSales and salesUnits
            - Return the output spark dataframe
        -----------------
    """
    def aggregate(self, joined_df):
        joined_df = joined_df.groupBy(["unique_key","channel","division","gender","category","weekofyear"])\
                .agg(F.sum("netSales").alias("netSales"), F.sum("salesUnits").alias("salesUnits"))
        out = joined_df.groupBy(["unique_key"])\
                        .agg(
                            F.first(F.col("channel")).alias("channel"),
                            F.first(F.col("division")).alias("division"),
                            F.first(F.col("gender")).alias("gender"),
                            F.first(F.col("category")).alias("category"),
                            F.array(F.struct( 
                            F.struct( 
                                F.lit("netSales").alias("rowId"), 
                                F.map_from_arrays( 
                                    F.collect_list("weekofyear"), 
                                    F.collect_list("netSales")
                                ).alias("dataRow") 
                            ).alias("dataRow"), 
                            F.struct( 
                                F.lit("salesUnits").alias("rowId"), 
                                F.map_from_arrays( 
                                    F.collect_list("weekofyear"), 
                                    F.collect_list("salesUnits")
                                ).alias("dataRow") 
                            ).alias("dataRow") 
                            )).alias("dataRows") 
                        ) \
                    .select("unique_key","channel","division","gender","category","dataRows")
        return out
        