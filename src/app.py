import json
from spark_etl import spark_etl
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--source', type=str, help='csv files source folder', required=True)
    parser.add_argument('--dest', type=str, help='json file destination folder', required=True)
    args = parser.parse_args()

    transformed_df = spark_etl().extract(args.source, args.dest)

